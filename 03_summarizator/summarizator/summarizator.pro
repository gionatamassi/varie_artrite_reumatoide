#-------------------------------------------------
#
# Project created by QtCreator 2015-03-16T10:48:53
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = summarizator
CONFIG   += console
CONFIG   -= app_bundle

OCV_PREFIX=opencv_
OCV_LIB = core \
          contrib \
          highgui \
          imgproc \
          video
unix {
    for(ocvlib, OCV_LIB) {
        LIBS += -l$$OCV_PREFIX$$ocvlib
    }
    # I'm compiling for myself... i.e. for my core-avx2
    QMAKE_CXXFLAGS+=-march=core-avx2
    #QMAKE_CFLAGS_AVX2
}

TEMPLATE = app

SOURCES += main.cpp

CONFIG += c++11
QMAKE_CXXFLAGS += -Wno-reorder

HEADERS += 

INCLUDEPATH+=
