#include <QCoreApplication>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDebug>
#include <QMultiMap>

#include <iostream>

struct Parameter {
    QString fileName;
    struct Roi {
        int x;
        int y;
        int w;
        int h;
    } roi;
};

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/contrib/contrib.hpp> // applyColorMap

struct RtImages {
    // unregistered
    cv::Mat sonography;
    cv::Mat boneMask;
    // registered
    cv::Mat dopplerSignal;
    cv::Mat boneSignal;
} rtImages;

struct SummaryImages {
    //std::vector< cv::Mat_< float > > boneSignal;
    //std::vector< cv::Mat_< float > > dopplerSignal;
    cv::Mat_< float > boneSignal;
    cv::Mat_< float > dopplerSignal;
    cv::Mat boneAndPD;

    cv::Mat summarization;
    cv::Mat minRed;
    cv::Mat maxRed;
    unsigned int minRedValue = -1;
    unsigned int maxRedValue = 0;
} summaryImages;

/** Pay attention: context has a global scope!
 * It should be encapsulated in a subsequent iteration.
 * @brief The Context struct
 */
struct Context {

    struct Video {
        uint idVideo;
        char path[255];
        uint noFrames;
        uint durationMs;
        uint fileSize;
        char fileName[255];
        char mimeType[255];
        char container[255];
    } video;
    struct Analysis {
        uint idAnalysis;
        uint xROI;
        uint yROI;
        uint wROI;
        uint hROI;
    } analysis;
    struct AnalyzedFrame {
        uint idAnalyzedFrame;
        uint timestamp;
        uint sequenceNo;
    } frame;
    struct DopplerSignal {
        uint idDopplerSignal;
    } pd;
    cv::VideoWriter *outputVideoPtr;
} context;

struct Blob {
    float lenght;
    float area;
    float compactness;
    float solidity;
    float roundness;
    float formFactor;

};

cv::Mat *krnlBoundEnhancer;

void imwrite_frame(const char *baseName, const char *ext, const uint frameNo, cv::Mat &frame) {
    return;
    // pay attention: the compiler have to support dynamic sized array... alloca!!!
    int baseNameLen = strlen(baseName);
    char fileName[baseNameLen+4+4+1]; // 4 number, 4 ext (.png), 1 null
    strcat(fileName, baseName);
    sprintf(fileName+baseNameLen, "%04d", frameNo);
    char *fn = fileName+baseNameLen+4;
    for (int j = 0; ext[j]; j++)
        fn[j] = ext[j];
    fn[4] = '\0';
    std::cout << fileName << std::endl;
    imwrite(fileName, frame);
}

/**
 * @brief extractPowerDopplerBlob extract the power doppler signal.
 * @param img is the original image from the sonographer
 * @param bMode is the B-mode scan, filled in black where a flow is marked
 * @param maks is the binary mask of the doppler signal
 * @param cielabImg is the frame in cielab
 * @return the power doppler signal
 */
cv::Mat extractPowerDopplerBlob(cv::Mat& img, cv::Mat& bMode, cv::Mat &mask, cv::Mat &cielabImg) {

    // prerequisite: a 24-bit BGR image and a 24-bit Lab image
    assert (img.type() == CV_8UC3);
    assert(cielabImg.type() == CV_8UC3);

    // bMode contains the B-mode scan
    cv::cvtColor(img, bMode, CV_BGR2GRAY);
    cv::Mat boneMask, inverseBone;
    cv::inRange(cielabImg, std::vector<uchar> {0, 115, 115}, std::vector<uchar> {255, 141, 141}, boneMask);
    cv::bitwise_not ( boneMask, inverseBone);
    cv::Mat reducedCielab;
    cielabImg.copyTo(reducedCielab, inverseBone);
    cv::inRange(reducedCielab, std::vector<uchar> {0, 115, 138}, std::vector<uchar> {250, 195, 197}, mask);
    cv::morphologyEx(mask, mask,cv::MORPH_OPEN,cv::Mat::ones(3,3,CV_8UC1), cv::Point(-1, -1), 3);

    assert(bMode.type() == CV_8UC1);
    assert(mask.type() == CV_8UC1);

    cv::Mat pd = cv::Mat::zeros(img.rows, img.cols, CV_8UC1);
    bMode.copyTo(pd, mask);
    bMode.copyTo(bMode, boneMask);
    cv::Mat integral;
    cv::integral(pd, integral);
    int redPixelIntegral = integral.at<int>(img.rows-1, img.cols-1);
    if (redPixelIntegral < summaryImages.minRedValue) {
        summaryImages.minRedValue = redPixelIntegral;
        img.copyTo(summaryImages.minRed);
    }
    if (redPixelIntegral > summaryImages.maxRedValue) {
        summaryImages.maxRedValue = redPixelIntegral;
        img.copyTo(summaryImages.maxRed);
    }
    printf("%4d\t%10d\n", context.frame.sequenceNo, redPixelIntegral);
    fprintf(stderr, "%4d/%4d\t%3d%%\n", context.frame.sequenceNo, context.video.noFrames, 100 * context.frame.sequenceNo / context.video.noFrames);

    return (pd);
}

void blob_extractor(cv::Mat &powerDopplerSignal)
{
    // Use a class or a function
    {
        std::vector < std::vector<cv::Point> > contours;
        cv::findContours(powerDopplerSignal, contours, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);
        for (size_t contourIdx = 0; contourIdx < contours.size(); contourIdx++)
        {
            /*
            cv::Mat blob(contours[contourIdx]);
            cv::drawContours(blob, contours, contourIdx, 255, CV_FILLED);
            cv::imshow("blob", blob);
            */
            cv::Moments moms = moments(cv::Mat(contours[contourIdx]));
            double area = moms.m00;
            if(area < 1.0)
                continue;
            double perimeter = cv::arcLength(cv::Mat(contours[contourIdx]), true);
            std::vector < cv::Point > hull;
            cv::convexHull(cv::Mat(contours[contourIdx]), hull);
            cv::Mat blobHull(hull);
            double hullArea = cv::contourArea(blobHull);

            cv::Point2d location = cv::Point2d(moms.m10 / moms.m00, moms.m01 / moms.m00);
            long int intensity = cv::sum(blobHull)[0];

            //compute blob radius
            double radius;
            {
                std::vector<double> dists;
                for (size_t pointIdx = 0; pointIdx < contours[contourIdx].size(); pointIdx++)
                {
                    cv::Point2d pt = contours[contourIdx][pointIdx];
                    dists.push_back(cv::norm(location - pt));
                }
                std::sort(dists.begin(), dists.end());
                radius = (dists[(dists.size() - 1) / 2] + dists[dists.size() / 2]) / 2.;
            }
            qDebug() << "Id: " << contourIdx;
            qDebug() << "Perimeter" << perimeter;
            qDebug() << "Area: " << area;
            qDebug() << "HullArea: " << hullArea;
            qDebug() << "Intensity: " << intensity;
            qDebug() << "Radius: " << radius;
        }
    }
    //exit(1);
}

void verticalLineEnhancement(cv::Mat& src) {
    int cols = src.cols;
    int rows = src.rows;
    for (int x = 0; x < cols; x++) {
        cv::Rect boundingBox(x, 0, 1, rows);
        cv::Mat line(src, boundingBox);
        cv::Scalar mean = cv::mean(line);
        line -= mean.val[0];
    }
}

void verticalLineCleaner(cv::Mat& src) {
    int diffLevel = 7; //< Warning: this ia a critical parameter
    //  and should be carefully calibrated
    const int rows = src.rows;
    const int cols = src.cols;
    const size_t step = src.step;
    uchar *const base = src.data;

    for (int x = 0; x < cols; x++) {
        int min = rows;
        int max = 0;
        uchar *level, *neighLevel;

        // find the first steep edge and set the min
        neighLevel = base + x;
        for (int y = 1; y < rows; y++, neighLevel= level) {
            level = neighLevel + step;
            if ((*level - *neighLevel) > diffLevel) {
                min = (neighLevel - base) / step;
                break;
            }
        }
        // if the upper edge has been found, look for the lower
        if (min > rows)
            break;
        neighLevel = base + x + rows * step;
        for (int y = rows; y > min; y--, neighLevel= level) {
            level = neighLevel - step;
            if ((*level - *neighLevel) > diffLevel) {
                max = (neighLevel - base) / step;
                break;
            }
        }
        // clean every pixel (between min and max)
        //for (int r = min + 1; r < max - 1; r++)
        for (int r = 0; r < rows; r++)
            *(base + x + r * step) = 0;
        // enforce min and max
        *(base + x + min * step) = 255;
        *(base + x + max * step) = 255;
        //std::cout << "col: " << x << "\tmin: " << min << "\tmax: " << max << std::endl;
    }
}

void findBone( cv::Mat& bScan, cv::Mat& boneMask, cv::Mat& bone) {
    // work on the half bottom part of the image
    cv::Rect thRect(0, 0, bScan.cols, bScan.rows - bScan.rows / 2);
    cv::Mat top = boneMask(thRect);
    cv::Rect bhRect(0, bScan.rows / 2, bScan.cols, bScan.rows / 2);
    cv::Mat bottom = boneMask(bhRect);
    top = cv::Mat::zeros(bScan.rows - bScan.rows / 2, bScan.cols, CV_8UC1);

    int kernelAvgSize = 7;
    cv::medianBlur(bScan(bhRect),bottom,kernelAvgSize);

    cv::filter2D(bottom, bottom, bScan.depth(), *krnlBoundEnhancer);
    cv::blur(bottom,bottom,cv::Size(3,3));
    for (int rep = 0; rep < 15; rep++) {
        verticalLineEnhancement(bottom);
    }
    assert (boneMask.type() == CV_8UC1);

    cv::Canny(bottom, bottom, 200, 250);
    cv::morphologyEx(bottom, bottom,cv::MORPH_CLOSE,cv::Mat::ones(3,3,CV_8UC1), cv::Point(-1, -1), 3);
    //cv::imshow("morph", bottom);

    std::vector<std::vector<cv::Point> > contours;
    cv::findContours( bottom, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
    int idx = 0;
    for (std::vector<std::vector<cv::Point> >::iterator c_iter = contours.begin();
         c_iter != contours.end(); c_iter++) {
        double area = cv::contourArea(*c_iter);
        // Filter by Area
#define MIN_AREA 50
        if (area < MIN_AREA) {
            contours.erase(c_iter--);
            continue;
        }
        /*
        // Get the moment
        cv::Moments mu;
        mu = moments( *c_iter, false );
        //  Get the mass center:
        cv::Point2f mc;
        mc = cv::Point2f( mu.m10/mu.m00 , mu.m01/mu.m00 );
        //qDebug() << mc.x << " " << mc.y;
        */
        idx++;
    }
    //verticalLineCleaner(bone);
    cv::drawContours(bottom, contours, -1, cv::Scalar::all(255),CV_FILLED);
    bone = cv::Mat::zeros(bScan.rows, bScan.cols, CV_8UC1);
    bScan.copyTo(bone, boneMask);

}

void findSkeleton(cv::Mat &src, cv::Mat &dst) {
    cv::Mat img;
    cv::threshold(src, img, 0, 255, cv::THRESH_BINARY | CV_THRESH_OTSU);
    cv::Mat *skel = new cv::Mat(img.size(), CV_8UC1, cv::Scalar(0));
    cv::Mat temp;
    cv::Mat eroded;

    cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));

    bool done;
    do
    {
        cv::erode(img, eroded, element);
        cv::dilate(eroded, temp, element); // temp = open(img)
        cv::subtract(img, temp, temp);
        cv::bitwise_or(*skel, temp, *skel);
        eroded.copyTo(img);

        done = (cv::countNonZero(img) == 0);
    } while (!done);
    dst = *skel;
}

void compose(
        cv::Mat &screen,
        cv::Mat &sonographyRt, cv::Mat &sonographySum,
        cv::Mat &dRt, cv::Mat &dSum,
        cv::Mat &boneRt, cv::Mat &boneSum) {
    int w = sonographyRt.cols,
        h = sonographyRt.rows;
    // real time frame
    cv::Rect scr0r = cv::Rect(0, 0, w, h);
    cv::Mat scr0 = screen(scr0r);
    sonographyRt.copyTo(scr0);
    // real time power doppler
    cv::Rect scr2r = cv::Rect(0, h, w, h);
    cv::Mat scr2 = screen(scr2r);
    dRt.copyTo(scr2);
    // real time bone
    cv::Rect scr4r = cv::Rect(0, 2*h, w, h);
    cv::Mat scr4 = screen(scr4r);
    boneRt.copyTo(scr4);
    // summarization, complete
    cv::Rect scr1r = cv::Rect(w, 0, w, h);
    cv::Mat scr1 = screen(scr1r);
    sonographySum.copyTo(scr1);
    // summariation, power doppler
    cv::Rect scr3r = cv::Rect(w, h, w, h);
    cv::Mat scr3 = screen(scr3r);
    dSum.copyTo(scr3);
    // summarization, bone
    cv::Rect scr5r = cv::Rect(w, 2*h, w, h);
    cv::Mat scr5 = screen(scr5r);
    boneSum.copyTo(scr5);

    cv::line(screen, cv::Point(0,   0), cv::Point(2*w-1,   0), cv::Scalar(255,255,255));
    cv::line(screen, cv::Point(0,   h), cv::Point(2*w-1,   h), cv::Scalar(255,255,255));
    cv::line(screen, cv::Point(0, 2*h), cv::Point(2*w-1, 2*h), cv::Scalar(255,255,255));
    cv::line(screen, cv::Point(0, 3*h-1), cv::Point(2*w-1, 3*h-1), cv::Scalar(255,255,255));
    cv::line(screen, cv::Point(  0, 0), cv::Point(  0, 3*h-1), cv::Scalar(255,255,255));
    cv::line(screen, cv::Point(  w, 0), cv::Point(  w, 3*h-1), cv::Scalar(255,255,255));
    cv::line(screen, cv::Point(2*w-1, 0), cv::Point(2*w-1, 3*h-1), cv::Scalar(255,255,255));

    cv::imshow("out", screen);
    *(context.outputVideoPtr) << screen;
    cv::waitKey(5);
}

void putHeading(cv::Mat img, std::string text1, std::string text2, int right = 0) {
    int fontFace = CV_FONT_HERSHEY_SIMPLEX;
    double fontScale = 2;
    int thickness = 1;
    int baseline=0;

    cv::Size textSize = cv::getTextSize(text1, fontFace,
                                fontScale, thickness, &baseline);
    baseline += thickness;
    // center the text1
    cv::Point textOrg((img.cols - textSize.width)/2,
                  (img.rows + textSize.height)/2 - 1.5*baseline);
    cv::putText(img, text1, textOrg, fontFace, fontScale,
            cv::Scalar::all(255), thickness, 8);
    textSize = cv::getTextSize(text2, fontFace,
                                fontScale, thickness, &baseline);
    baseline += thickness;

    // center the text2
    textOrg.x = (img.cols - textSize.width)/2;
    textOrg.y = (img.rows + textSize.height)/2 + 1.5*baseline;

    // then put the text itself
    cv::putText(img, text2, textOrg, fontFace, fontScale,
            cv::Scalar::all(255), thickness, 8);

    // box
    cv::Scalar color;
    if (right) {
        color = cv::Scalar(0, 255, 0);
    } else {
        color = cv::Scalar(0, 0, 255);
    }
    cv::rectangle(img, cv::Point(0, 0), cv::Point(img.cols, img.rows), color, 3);
}

void sceneHeadings( int width, int height, cv::Mat &frame) {
    cv::Mat sonographyRt(height, width, CV_8UC3, cv::Scalar::all(0)),
            sonographySummary(height, width, CV_8UC3, cv::Scalar::all(0)),
            dopplerSignalRt(height, width, CV_8UC3, cv::Scalar::all(0)),
            dopplerSignalSummary(height, width, CV_8UC3, cv::Scalar::all(0)),
            boneSignalRt(height, width, CV_8UC3, cv::Scalar::all(0)),
            boneSignalSummary(height, width, CV_8UC3, cv::Scalar::all(0));

    putHeading(sonographyRt, "Sonography", "real time", 0);
    putHeading(sonographySummary, "Sonography", "summary", 1);
    putHeading(dopplerSignalRt, "Doppler signal", "real time", 0);
    putHeading(dopplerSignalSummary, "Doppler signal", "summary", 1);
    putHeading(boneSignalRt, "Bone", "real time", 0);
    putHeading(boneSignalSummary, "Bone", "summary", 1);
    for (int j = 0; j < 125; j++) {
        compose( frame,
                 sonographyRt, sonographySummary,
                 dopplerSignalRt, dopplerSignalSummary,
                 boneSignalRt, boneSignalSummary);
    }
}

void createVideoFrame(cv::Mat &screen) {
    cv::Mat dRt = cv::Mat::zeros(rtImages.sonography.rows, rtImages.sonography.cols, CV_8UC3),
            bRt = cv::Mat::zeros(rtImages.sonography.rows, rtImages.sonography.cols, CV_8UC3),
            pdSum8bit, boneSum8bit, tmp,
            dopplerSum, boneSum;
    cv::applyColorMap(rtImages.dopplerSignal, dRt, cv::COLORMAP_HOT);
    cv::cvtColor(rtImages.boneSignal, bRt, CV_GRAY2BGR);

    // doppler summarization
    tmp = summaryImages.dopplerSignal / (context.frame.sequenceNo + 1);
    tmp.convertTo(pdSum8bit, CV_8U);
    cv::applyColorMap(pdSum8bit, dopplerSum, cv::COLORMAP_HOT);

    tmp = summaryImages.boneSignal / (context.frame.sequenceNo + 1);
    tmp.convertTo(boneSum8bit, CV_8U);
    cv::cvtColor(boneSum8bit, boneSum, CV_GRAY2BGR);

    cv::add(dopplerSum, boneSum, summaryImages.summarization);

    compose(screen, rtImages.sonography, summaryImages.summarization,
                    dRt, dopplerSum,
                    bRt, boneSum);
}

void summarize() {
    cv::Mat pdFloat;
    rtImages.dopplerSignal.convertTo(pdFloat, CV_64FC1);
    cv::Mat boneFloat;
    rtImages.boneSignal.convertTo(boneFloat, CV_64FC1);
    //cv::addWeighted(boneFloat, 2./3, rtImages.boneSignal, 1./3, 0., boneFloat);
    if (context.frame.sequenceNo == 0) {
        summaryImages.dopplerSignal = pdFloat.clone();
        summaryImages.boneSignal = boneFloat.clone();
    } else {
        cv::add(summaryImages.dopplerSignal, pdFloat, summaryImages.dopplerSignal);
        cv::add(summaryImages.boneSignal, boneFloat, summaryImages.boneSignal);
    }
}

void process(Parameter &p) {
    // the OpenCV video capture object
    cv::VideoCapture capture(p.fileName.toStdString());
    p.roi.x = p.roi.y = 0;
    p.roi.w = capture.get(CV_CAP_PROP_FRAME_WIDTH);
    p.roi.h = capture.get(CV_CAP_PROP_FRAME_HEIGHT);

    // the bounding box
    cv::Rect boundingBox(p.roi.x, p.roi.y, p.roi.w, p.roi.h);
    if (!capture.isOpened()) {
        exit (1);
    }

    //! the video frame
    cv::Mat frame;
    //! the b-mode
    cv::Mat Bmode;
    //! a mask of the power doppler signal
    cv::Mat pdMask(boundingBox.height, boundingBox.width, CV_8UC1);
    cv::Mat boneMask(boundingBox.height, boundingBox.width, CV_8UC1);
    //! The image in the CIE L*a*b* space
    cv::Mat cielabImg;
    // the video frame
    cv::Mat screen(boundingBox.height*3, boundingBox.width*2, CV_8UC3);

    context.frame.sequenceNo = 0;

    cv::Size s = cv::Size((int) boundingBox.width*2,
                     (int) boundingBox.height*3);
    QString outFileName = p.fileName;
    outFileName.replace(".avi", "_processed.avi");
    // int ex = static_cast<int>(capture.get(CV_CAP_PROP_FOURCC));
    context.video.noFrames = capture.get(CV_CAP_PROP_FRAME_COUNT);
    int ex = 877677894;
    try {
        context.outputVideoPtr = new cv::VideoWriter(outFileName.toStdString(), ex, capture.get(CV_CAP_PROP_FPS), s, true);
    } catch( cv::Exception& e ) {
        const char* err_msg = e.what();
        std::cout << "exception caught: " << err_msg << std::endl;
    }

    if (!context.outputVideoPtr->isOpened()) {
        std::cout  << "Could not open the output video for write: " << outFileName.toStdString() << std::endl;
        exit (1);
    }

    sceneHeadings(boundingBox.width, boundingBox.height, screen);

    // while there exists a next frame
    while (capture.read(frame)) {
        rtImages.sonography = cv::Mat(frame, boundingBox);
        cv::cvtColor(rtImages.sonography, cielabImg, CV_BGR2Lab);
        // get the artifacts and remove them from the frame
        rtImages.dopplerSignal = extractPowerDopplerBlob(rtImages.sonography, Bmode, pdMask, cielabImg);
        findBone(Bmode, boneMask, rtImages.boneSignal);
        summarize();
        createVideoFrame(screen);

        context.frame.sequenceNo++;
    }
    capture.release();
    delete context.outputVideoPtr;
}

bool readParameters(Parameter &p, const char *fileName) {
    QFile configFile(fileName);

    if (!configFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open config file.");
        return false;
    }

    QByteArray configData = configFile.readAll();
    QJsonDocument configDoc(QJsonDocument::fromJson(configData));
    QJsonObject configObject = configDoc.object();

    p.fileName = configObject["fileName"].toString();
    QJsonObject roiObject = configObject["ROI"].toObject();
    p.roi.x = roiObject["x"].toInt();
    p.roi.y = roiObject["y"].toInt();
    p.roi.w = roiObject["w"].toInt();
    p.roi.h = roiObject["h"].toInt();

    return true;
}

void krnlBoundEnhancerSetup(void)
{
    /*
    krnlBoundEnhancer = new cv::Mat(2,2,CV_32F,cv::Scalar(1.0));
    krnlBoundEnhancer->at<float>(0,0) = -1.0;
    krnlBoundEnhancer->at<float>(0,1) = -1.0;
    krnlBoundEnhancer->at<float>(1,0) =  1.0;
    krnlBoundEnhancer->at<float>(1,1) =  1.0;
    */
    krnlBoundEnhancer = new cv::Mat(5,5,CV_32F,cv::Scalar(1.0));
    krnlBoundEnhancer->at<float>(0,0) = -2.0;
    krnlBoundEnhancer->at<float>(0,1) = -2.0;
    krnlBoundEnhancer->at<float>(0,2) = -2.0;
    krnlBoundEnhancer->at<float>(0,3) = -2.0;
    krnlBoundEnhancer->at<float>(0,4) = -2.0;
    krnlBoundEnhancer->at<float>(1,0) = -1.0;
    krnlBoundEnhancer->at<float>(1,1) =  2.0;
    krnlBoundEnhancer->at<float>(1,2) =  4.0;
    krnlBoundEnhancer->at<float>(1,3) =  2.0;
    krnlBoundEnhancer->at<float>(1,4) = -1.0;
    krnlBoundEnhancer->at<float>(2,0) =  0.0;
    krnlBoundEnhancer->at<float>(2,1) =  4.0;
    krnlBoundEnhancer->at<float>(2,2) =  8.0;
    krnlBoundEnhancer->at<float>(2,3) =  4.0;
    krnlBoundEnhancer->at<float>(2,4) =  0.0;
    krnlBoundEnhancer->at<float>(3,0) = -1.0;
    krnlBoundEnhancer->at<float>(3,1) =  2.0;
    krnlBoundEnhancer->at<float>(3,2) =  4.0;
    krnlBoundEnhancer->at<float>(3,3) =  2.0;
    krnlBoundEnhancer->at<float>(3,4) = -1.0;
    krnlBoundEnhancer->at<float>(4,0) = -2.0;
    krnlBoundEnhancer->at<float>(4,1) = -2.0;
    krnlBoundEnhancer->at<float>(4,2) = -2.0;
    krnlBoundEnhancer->at<float>(4,3) = -2.0;
    krnlBoundEnhancer->at<float>(4,4) = -2.0;
    int norm = 0;
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            norm+= krnlBoundEnhancer->at<float>(i,j);
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            krnlBoundEnhancer->at<float>(i,j)/=norm;
    return;
}

int main(int argc, char *argv[])
{
    Parameter p;
    const char *fileName;

    if (argc < 2) {
        qWarning(QString("Usage: %1 <config.json>").arg(argv[0]).toLocal8Bit());
        fileName = "config.json";
    } else {
        fileName = argv[1];
    }
    if (!readParameters(p, fileName))
        return 1;

    //Context context;
    krnlBoundEnhancerSetup();
    process(p);

    p.fileName.replace(".avi", "");
    QString optimistic = p.fileName + "_optimistic.ppm";
    QString pessimistic = p.fileName + "_pessimistic.ppm";
    QString summaritazion = p.fileName + "_summarization.ppm";
    cv::imwrite(optimistic.toStdString(), summaryImages.minRed);
    cv::imwrite(pessimistic.toStdString(), summaryImages.maxRed);
    cv::imwrite(summaritazion.toStdString(), summaryImages.summarization);

    return 0;
}
