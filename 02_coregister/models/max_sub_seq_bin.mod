param N >= 0;
param rzero >= 0;
set Frames := 1..N;
set F2 := 2..N;
set FN := 1..N-1;
set Couples := {i in 1..N-1, j in 2..N : j > i };
param d { (i, j) in Couples };

var s {Frames} binary;
var e {Frames} binary;
var b {Frames} binary;
var r >= 0;

maximize L:
	sum {f in Frames} b[f];

subject to start { f in F2 } :
	s[f] >= s[f-1];

subject to end { f in FN } :
	e[f] >= e[f+1];

subject to belong { f in Frames } :
	b[f] = s[f] + e[f] - 1;

subject to max_distance { (h, k) in Couples } :
	r >= d[h, k]*(s[h] + e[k] - 1);

subject to coregistrable :
	r <= rzero;