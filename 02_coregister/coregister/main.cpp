#include <QCoreApplication>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QFileInfo>
#include <QDebug>
#include <QTextStream>
#include <iostream>

//#include "reg/include/opencv2/reg/mapprojec.hpp"
#include "reg/include/opencv2/reg/mapaffine.hpp"
#include "reg/include/opencv2/reg/mappergradeuclid.hpp"
//#include "reg/include/opencv2/reg/mappergradproj.hpp"
//#include "reg/include/opencv2/reg/mappergradaffine.hpp"
#include "reg/include/opencv2/reg/mapperpyramid.hpp"

struct Parameter {
    QString fileName;
    struct Roi {
        int x;
        int y;
        int w;
        int h;
    } roi;
    struct Range {
        int min;
        int max;
    } range;
} parameter;

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/contrib/contrib.hpp> // applyColorMap

struct RtImages {
    // unregistered
    cv::Mat sonography;
    cv::Mat boneMask;
    cv::Mat bScan;
    // registered
    cv::Mat dopplerSignal;
    cv::Mat boneSignal;
    cv::Mat sonographyReg;
} rtImages;

struct SummaryImages {
    //std::vector< cv::Mat_< float > > boneSignal;
    //std::vector< cv::Mat_< float > > dopplerSignal;
    cv::Mat_< float > boneSignal;
    cv::Mat_< float > dopplerSignal;
    cv::Mat boneAndPD;
} summaryImages;

typedef cv::Vec<double, 2> Origin;

struct RegistrationImages {
    cv::Mat templateBone;
    cv::Mat templateBScan;
    cv::reg::MapAffine mapAffine;
    cv::vector< Origin > origins;
} registrationImages;

/** Pay attention: context has a global scope!
 * It should be encapsulated in a subsequent iteration.
 * @brief The Context struct
 */
struct Context {

    struct Video {
        uint idVideo;
        char path[255];
        uint noFrames;
        uint durationMs;
        uint fileSize;
        char fileName[255];
        char mimeType[255];
        char container[255];
    } video;
    struct Analysis {
        uint idAnalysis;
        uint xROI;
        uint yROI;
        uint wROI;
        uint hROI;
    } analysis;
    struct AnalyzedFrame {
        uint idAnalyzedFrame;
        uint timestamp;
        uint sequenceNo;
    } frame;
    struct DopplerSignal {
        uint idDopplerSignal;
    } pd;
    //cv::VideoWriter *outputVideoPtr;
} context;

struct Blob {
    float lenght;
    float area;
    float compactness;
    float solidity;
    float roundness;
    float formFactor;

};

cv::Mat *krnlBoundEnhancer;

void imwrite_frame(const char *baseName, const char *ext, const uint frameNo, cv::Mat &frame) {
    return;
    // pay attention: the compiler have to support dynamic sized array... alloca!!!
    int baseNameLen = strlen(baseName);
    char fileName[baseNameLen+4+4+1]; // 4 number, 4 ext (.png), 1 null
    strcat(fileName, baseName);
    sprintf(fileName+baseNameLen, "%04d", frameNo);
    char *fn = fileName+baseNameLen+4;
    for (int j = 0; ext[j]; j++)
        fn[j] = ext[j];
    fn[4] = '\0';
    std::cout << fileName << std::endl;
    imwrite(fileName, frame);
}

/**
 * @brief extractPowerDopplerBlob extract the power doppler signal.
 * @param img is the original image from the sonographer
 * @param bMode is the B-mode scan, filled in black where a flow is marked
 * @param maks is the binary mask of the doppler signal
 * @param cielabImg is the frame in cielab
 * @return the power doppler signal
 */
cv::Mat extractPowerDopplerBlob(cv::Mat& img, cv::Mat& bMode, cv::Mat &mask, cv::Mat &cielabImg) {

    // prerequisite: a 24-bit BGR image and a 24-bit Lab image
    assert (img.type() == CV_8UC3);
    assert(cielabImg.type() == CV_8UC3);

    // bMode contains the B-mode scan
    cv::cvtColor(img, bMode, CV_BGR2GRAY);
    cv::Mat boneMask, inverseBone;
    cv::inRange(cielabImg, std::vector<uchar> {0, 115, 115}, std::vector<uchar> {255, 141, 141}, boneMask);
    cv::bitwise_not ( boneMask, inverseBone);
    cv::Mat reducedCielab;
    cielabImg.copyTo(reducedCielab, inverseBone);
    cv::inRange(reducedCielab, std::vector<uchar> {0, 115, 138}, std::vector<uchar> {250, 195, 197}, mask);
    cv::morphologyEx(mask, mask,cv::MORPH_OPEN,cv::Mat::ones(3,3,CV_8UC1), cv::Point(-1, -1), 3);

    assert(bMode.type() == CV_8UC1);
    assert(mask.type() == CV_8UC1);

    cv::Mat pd;
    bMode.copyTo(pd, mask);
    bMode.copyTo(bMode, boneMask);

    return (pd);
}

void verticalLineEnhancement(cv::Mat& src) {
    int cols = src.cols;
    int rows = src.rows;
    for (int x = 0; x < cols; x++) {
        cv::Rect boundingBox(x, 0, 1, rows);
        cv::Mat line(src, boundingBox);
        cv::Scalar mean = cv::mean(line);
        line -= mean.val[0];
    }
}

void verticalLineCleaner(cv::Mat& src) {
    int diffLevel = 7; //< Warning: this ia a critical parameter
    //  and should be carefully calibrated
    const int rows = src.rows;
    const int cols = src.cols;
    const size_t step = src.step;
    uchar *const base = src.data;

    for (int x = 0; x < cols; x++) {
        int min = rows;
        int max = 0;
        uchar *level, *neighLevel;

        // find the first steep edge and set the min
        neighLevel = base + x;
        for (int y = 1; y < rows; y++, neighLevel= level) {
            level = neighLevel + step;
            if ((*level - *neighLevel) > diffLevel) {
                min = (neighLevel - base) / step;
                break;
            }
        }
        // if the upper edge has been found, look for the lower
        if (min > rows)
            break;
        neighLevel = base + x + rows * step;
        for (int y = rows; y > min; y--, neighLevel= level) {
            level = neighLevel - step;
            if ((*level - *neighLevel) > diffLevel) {
                max = (neighLevel - base) / step;
                break;
            }
        }
        // clean every pixel (between min and max)
        //for (int r = min + 1; r < max - 1; r++)
        for (int r = 0; r < rows; r++)
            *(base + x + r * step) = 0;
        // enforce min and max
        *(base + x + min * step) = 255;
        *(base + x + max * step) = 255;
        //std::cout << "col: " << x << "\tmin: " << min << "\tmax: " << max << std::endl;
    }
}

void findBone( cv::Mat& src, cv::Mat& dst) {
    // work on the half bottom part of the image
    cv::Rect thRect(0, 0, src.cols, src.rows - src.rows / 2);
    cv::Mat top = dst(thRect);
    cv::Rect bhRect(0, src.rows / 2, src.cols, src.rows / 2);
    cv::Mat bottom = dst(bhRect);
    top = cv::Mat::zeros(src.rows - src.rows / 2, src.cols, CV_8UC1);

    int kernelAvgSize = 7;
    cv::medianBlur(src(bhRect),bottom,kernelAvgSize);

    cv::filter2D(bottom, bottom, src.depth(), *krnlBoundEnhancer);
    cv::blur(bottom,bottom,cv::Size(3,3));
    for (int rep = 0; rep < 15; rep++) {
        verticalLineEnhancement(bottom);
    }
    assert (dst.type() == CV_8UC1);

    cv::Canny(bottom, bottom, 200, 250);
    cv::morphologyEx(bottom, bottom,cv::MORPH_CLOSE,cv::Mat::ones(3,3,CV_8UC1), cv::Point(-1, -1), 3);
    //cv::imshow("morph", bottom);

    std::vector<std::vector<cv::Point> > contours;
    cv::findContours( bottom, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
    int idx = 0;
    for (std::vector<std::vector<cv::Point> >::iterator c_iter = contours.begin();
         c_iter != contours.end(); c_iter++) {
        double area = cv::contourArea(*c_iter);
        // Filter by Area
#define MIN_AREA 50
        if (area < MIN_AREA) {
            contours.erase(c_iter--);
            continue;
        }
        /*
        // Get the moment
        cv::Moments mu;
        mu = moments( *c_iter, false );
        //  Get the mass center:
        cv::Point2f mc;
        mc = cv::Point2f( mu.m10/mu.m00 , mu.m01/mu.m00 );
        //qDebug() << mc.x << " " << mc.y;
        */
        idx++;
    }
    //verticalLineCleaner(bone);
    cv::drawContours(bottom, contours, -1, cv::Scalar::all(255),CV_FILLED);

    /*
    kmeansSegment kmeans(4);
    cv::Mat result = kmeans.segment(contorno);
    cv::Mat binary;
    cv::Canny(result, binary, 30, 90);
    cv::imshow("original", src);
    cv::imshow("binary", binary);
    cv::imshow( "clustered image", result);
    cv::imwrite("kmeans_binary.jpg", binary);
    cv::imwrite("kmeans.jpg", result);
    */
}

void showDifference(const cv::Mat& image1, const cv::Mat& image2, const char* title)
{
    cv::Mat img1, img2;
    image1.convertTo(img1, CV_32FC3);
    image2.convertTo(img2, CV_32FC3);
    if(img1.channels() != 1)
        cv::cvtColor(img1, img1, CV_RGB2GRAY);
    if(img2.channels() != 1)
        cv::cvtColor(img2, img2, CV_RGB2GRAY);

    cv::Mat imgDiff;
    img1.copyTo(imgDiff);
    imgDiff -= img2;
    imgDiff /= 2.f;
    imgDiff += 128.f;

    cv::Mat imgSh;
    imgDiff.convertTo(imgSh, CV_8UC3);
    cv::imshow(title, imgSh);
}


void appendVideoFrame() {
    cv::imshow("original", rtImages.sonography);
    cv::imshow("registerd", rtImages.sonographyReg);
    // //showDifference(rtImages.sonography, rtImages.sonographyReg, "diff");
    //*(context.outputVideoPtr) << rtImages.sonographyReg;
    cv::waitKey(50);
}

// Register
cv::reg::MapperGradEuclid mapper;
cv::reg::MapperPyramid mappPyr(mapper);
cv::Ptr<cv::reg::Map> mapPtr;

void imageRegister() {
    if (context.frame.sequenceNo == 0 || context.frame.sequenceNo == parameter.range.min) {
        rtImages.sonography.copyTo(registrationImages.templateBScan,
                                   rtImages.bScan);
        // Convert to double, 3 channels
        registrationImages.templateBScan.convertTo(registrationImages.templateBScan, CV_64FC3);
        rtImages.sonography.copyTo(rtImages.sonographyReg);
        cv::Matx<double, 2, 2> A;
        cv::Vec<double, 2> b;
        A(0, 0) = A(1, 1) = 1.0;
        A(0, 1) = A(1, 0) = b(0) = b(1) = 0.0;
        registrationImages.mapAffine = cv::reg::MapAffine(A, b);
        registrationImages.origins.push_back(b);
    } else {
        cv::Mat img2;
        rtImages.sonography.copyTo(img2,
                                   rtImages.bScan);
        // Convert to double, 3 channels
        img2.convertTo(img2, CV_64FC3);

        mappPyr.calculate(registrationImages.templateBScan,
                          img2,
                          mapPtr);

        cv::reg::MapAffine* mapAff = dynamic_cast<cv::reg::MapAffine*>(mapPtr.obj);
        double distance = pow(mapAff->getShift()[0]
                -registrationImages.mapAffine.getShift()[0], 2) +
           pow(mapAff->getShift()[1]
           -registrationImages.mapAffine.getShift()[1], 2);
        printf("%4d\t%10.8f\n", context.frame.sequenceNo, distance);

        registrationImages.mapAffine = *mapAff;
        //mapAff->normalize();
        //mapProj->inverseWarp(rtImages.boneSignal, rtImages.boneSignal);
        //mapProj->inverseWarp(rtImages.dopplerSignal, rtImages.dopplerSignal);
        //rtImages.sonographyReg = cv::Mat::zeros(rtImages.sonographyReg.size(), rtImages.sonographyReg.type());
        //std::cout << mapAff->getShift() << std::endl;
        //std::cout << mapAff->getLinTr() << std::endl;
        mapAff->inverseWarp(rtImages.sonography, rtImages.sonographyReg);
        registrationImages.origins.push_back(mapAff->getShift());
    }
}

void process() {
    Parameter &p = parameter;
    // the OpenCV video capture object
    cv::VideoCapture capture(p.fileName.toStdString());
    // the bounding box
    cv::Rect boundingBox(p.roi.x, p.roi.y, p.roi.w, p.roi.h);
    if (!capture.isOpened()) {
        exit (1);
    }

    //! the video frame
    cv::Mat frame;
    //! the b-mode
    cv::Mat Bmode;
    //! a mask of the power doppler signal
    cv::Mat pdMask(boundingBox.height, boundingBox.width, CV_8UC1);
    cv::Mat boneMask(boundingBox.height, boundingBox.width, CV_8UC1);
    //! The image in the CIE L*a*b* space
    cv::Mat cielabImg;
    context.video.noFrames = capture.get(CV_CAP_PROP_FRAME_COUNT);

    cv::Size s = cv::Size((int) boundingBox.width,
                     (int) boundingBox.height);
    QString outFileName = p.fileName;
    outFileName.replace(".avi", "_registered.avi");
    // int ex = static_cast<int>(capture.get(CV_CAP_PROP_FOURCC));
    int ex = 877677894;
    try {
        // context.outputVideoPtr = new cv::VideoWriter(outFileName.toStdString(), ex, capture.get(CV_CAP_PROP_FPS), s, true);
    } catch( cv::Exception& e ) {
        const char* err_msg = e.what();
        std::cout << "exception caught: " << err_msg << std::endl;
    }

    /*
    if (!context.outputVideoPtr->isOpened()) {
        std::cout  << "Could not open the output video for write: " << outFileName.toStdString() << std::endl;
        exit (1);
    }
    */

    // while there exists a next frame
    for (context.frame.sequenceNo = 0;
         capture.read(frame) && context.frame.sequenceNo <= p.range.max;
         context.frame.sequenceNo++) {

        if (context.frame.sequenceNo < p.range.min) {
            continue;
        }
        rtImages.sonography = cv::Mat(frame, boundingBox);
        cv::cvtColor(rtImages.sonography, cielabImg, CV_BGR2Lab);
        // get the artifacts and remove them from the frame
        rtImages.dopplerSignal = extractPowerDopplerBlob(rtImages.sonography, Bmode, pdMask, cielabImg);
        findBone(Bmode, boneMask);
        Bmode.copyTo(rtImages.boneSignal, boneMask);
        rtImages.bScan = Bmode;
        imageRegister();
        // appendVideoFrame();
    }
    capture.release();
    //delete context.outputVideoPtr;
}

bool readParameters(const char *fileName) {
    Parameter &p = parameter;
    QFile configFile(fileName);

    if (!configFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open config file.");
        return false;
    }

    QByteArray configData = configFile.readAll();
    QJsonDocument configDoc(QJsonDocument::fromJson(configData));
    QJsonObject configObject = configDoc.object();

    p.fileName = configObject["fileName"].toString();
    QJsonObject roiObject = configObject["ROI"].toObject();
    p.roi.x = roiObject["x"].toInt();
    p.roi.y = roiObject["y"].toInt();
    p.roi.w = roiObject["w"].toInt();
    p.roi.h = roiObject["h"].toInt();
    QJsonObject rangeObject = configObject["Range"].toObject();
    p.range.min = rangeObject["min"].toInt();
    p.range.max = rangeObject["max"].toInt();

    return true;
}

void krnlBoundEnhancerSetup(void)
{
    /*
    krnlBoundEnhancer = new cv::Mat(2,2,CV_32F,cv::Scalar(1.0));
    krnlBoundEnhancer->at<float>(0,0) = -1.0;
    krnlBoundEnhancer->at<float>(0,1) = -1.0;
    krnlBoundEnhancer->at<float>(1,0) =  1.0;
    krnlBoundEnhancer->at<float>(1,1) =  1.0;
    */
    krnlBoundEnhancer = new cv::Mat(5,5,CV_32F,cv::Scalar(1.0));
    krnlBoundEnhancer->at<float>(0,0) = -2.0;
    krnlBoundEnhancer->at<float>(0,1) = -2.0;
    krnlBoundEnhancer->at<float>(0,2) = -2.0;
    krnlBoundEnhancer->at<float>(0,3) = -2.0;
    krnlBoundEnhancer->at<float>(0,4) = -2.0;
    krnlBoundEnhancer->at<float>(1,0) = -1.0;
    krnlBoundEnhancer->at<float>(1,1) =  2.0;
    krnlBoundEnhancer->at<float>(1,2) =  4.0;
    krnlBoundEnhancer->at<float>(1,3) =  2.0;
    krnlBoundEnhancer->at<float>(1,4) = -1.0;
    krnlBoundEnhancer->at<float>(2,0) =  0.0;
    krnlBoundEnhancer->at<float>(2,1) =  4.0;
    krnlBoundEnhancer->at<float>(2,2) =  8.0;
    krnlBoundEnhancer->at<float>(2,3) =  4.0;
    krnlBoundEnhancer->at<float>(2,4) =  0.0;
    krnlBoundEnhancer->at<float>(3,0) = -1.0;
    krnlBoundEnhancer->at<float>(3,1) =  2.0;
    krnlBoundEnhancer->at<float>(3,2) =  4.0;
    krnlBoundEnhancer->at<float>(3,3) =  2.0;
    krnlBoundEnhancer->at<float>(3,4) = -1.0;
    krnlBoundEnhancer->at<float>(4,0) = -2.0;
    krnlBoundEnhancer->at<float>(4,1) = -2.0;
    krnlBoundEnhancer->at<float>(4,2) = -2.0;
    krnlBoundEnhancer->at<float>(4,3) = -2.0;
    krnlBoundEnhancer->at<float>(4,4) = -2.0;
    int norm = 0;
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            norm+= krnlBoundEnhancer->at<float>(i,j);
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            krnlBoundEnhancer->at<float>(i,j)/=norm;
    return;
}

void createModelData() {
    QFileInfo videoFile(parameter.fileName);
    QString fname = videoFile.baseName();
    fname.append(".dat");
    QFile dataFile(fname);
    if(!dataFile.open(QFile::WriteOnly | QFile::Text)) {
        qDebug() << " Could not open file for writing";
        return;
    }
    QString paramN = QString("param N := %1;\n").arg(parameter.range.max - parameter.range.min);
    QString paramRzero("param rzero := 5;\n");
    QString paramD("param d :=");
    for (int i = 1; i < (parameter.range.max - parameter.range.min); i++) {
        for (int j = i + 1; j < (parameter.range.max - parameter.range.min + 1); j++) {
            double distance = pow(registrationImages.origins[i-1][0]
                    -registrationImages.origins[j-1][0], 2) +
                    pow(registrationImages.origins[i-1][1]
                    -registrationImages.origins[j-1][1], 2);
            paramD.append(QString("\n[%1, %2] %3").arg(i).arg(j).arg(distance));
        }
    }
    paramD.append(";");
    QTextStream out(&dataFile);
    out << paramN;
    out << paramRzero;
    out << paramD;
    dataFile.flush();
    dataFile.close();
}

int main(int argc, char *argv[])
{
    Parameter p;
    const char *fileName;

    if (argc < 2) {
        qWarning(QString("Usage: %1 <config.json>").arg(argv[0]).toLocal8Bit());
        fileName = "config.json";
    } else {
        fileName = argv[1];
    }
    if (!readParameters(fileName))
        return 1;

    //Context context;
    krnlBoundEnhancerSetup();
    process();
    createModelData();

    return 0;
}
