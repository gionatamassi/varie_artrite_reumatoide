#-------------------------------------------------
#
# Project created by QtCreator 2015-03-16T10:48:53
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = coregister
CONFIG   += console
CONFIG   -= app_bundle

OCV_PREFIX=opencv_
OCV_LIB = core \
          contrib \
          highgui \
          imgproc \
          video
unix {
    for(ocvlib, OCV_LIB) {
        LIBS += -l$$OCV_PREFIX$$ocvlib
    }
    # I'm compiling for myself... i.e. for my core-avx2
    QMAKE_CXXFLAGS+=-march=core-avx2
    #QMAKE_CFLAGS_AVX2
}

TEMPLATE = app

SOURCES += main.cpp\
    reg/src/map.cpp \               # Registration
    reg/src/mapaffine.cpp \
    reg/src/mapper.cpp \
    reg/src/mappergradaffine.cpp \
    reg/src/mappergradeuclid.cpp \
    reg/src/mappergradproj.cpp \
    reg/src/mappergradshift.cpp \
    reg/src/mappergradsimilar.cpp \
    reg/src/mapperpyramid.cpp \
    reg/src/mapprojec.cpp \
    reg/src/mapshift.cpp
    
CONFIG += c++11
QMAKE_CXXFLAGS += -Wno-reorder

HEADERS += \
    reg/include/opencv2/reg/map.hpp \
    reg/include/opencv2/reg/mapaffine.hpp \
    reg/include/opencv2/reg/mapper.hpp \
    reg/include/opencv2/reg/mappergradaffine.hpp \
    reg/include/opencv2/reg/mappergradeuclid.hpp \
    reg/include/opencv2/reg/mappergradproj.hpp \
    reg/include/opencv2/reg/mappergradshift.hpp \
    reg/include/opencv2/reg/mappergradsimilar.hpp \
    reg/include/opencv2/reg/mapperpyramid.hpp \
    reg/include/opencv2/reg/mapprojec.hpp \
    reg/include/opencv2/reg/mapshift.hpp

INCLUDEPATH+=./reg/include
