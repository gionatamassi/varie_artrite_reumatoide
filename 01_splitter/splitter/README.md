This piece of code:

* read a config file
* open the video
* sets the specified ROI
* split each frame in power doppler signal and B-mode scan

The config file specifies:
* the file path and name
* the ROI:
    * x
    * y
    * w
    * h


1. Get the RGB and the La*b* image
2. for each pixel:
3. if all La*b* threshold are satisfied, label as power doppler and copy L
4. else, if RGB is quasi-gray, label as bone and copy L

