#include <QCoreApplication>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDebug>
#include <QMultiMap>

#include <iostream>

//#include "kmeanssegment.h"
#include "reg/include/opencv2/reg/mapprojec.hpp"
#include "reg/include/opencv2/reg/mappergradproj.hpp"
#include "reg/include/opencv2/reg/mapperpyramid.hpp"

struct Parameter {
    QString fileName;
    struct Roi {
        int x;
        int y;
        int w;
        int h;
    } roi;
};

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/contrib/contrib.hpp> // applyColorMap

struct RtImages {
    // unregistered
    cv::Mat sonography;
    cv::Mat boneMask;
    // registered
    cv::Mat dopplerSignal;
    cv::Mat boneSignal;
    cv::Mat sonographyReg;
} rtImages;

struct SummaryImages {
    //std::vector< cv::Mat_< float > > boneSignal;
    //std::vector< cv::Mat_< float > > dopplerSignal;
    cv::Mat_< float > boneSignal;
    cv::Mat_< float > dopplerSignal;
    cv::Mat boneAndPD;
} summaryImages;

struct RegistrationImages {
    cv::Mat templateBone;
    cv::reg::MapProjec mapProj;
} registrationImages;

/** Pay attention: context has a global scope!
 * It should be encapsulated in a subsequent iteration.
 * @brief The Context struct
 */
struct Context {

    struct Video {
        uint idVideo;
        char path[255];
        uint noFrames;
        uint durationMs;
        uint fileSize;
        char fileName[255];
        char mimeType[255];
        char container[255];
    } video;
    struct Analysis {
        uint idAnalysis;
        uint xROI;
        uint yROI;
        uint wROI;
        uint hROI;
    } analysis;
    struct AnalyzedFrame {
        uint idAnalyzedFrame;
        uint timestamp;
        uint sequenceNo;
    } frame;
    struct DopplerSignal {
        uint idDopplerSignal;
    } pd;
    cv::VideoWriter *outputVideoPtr;
} context;

struct Blob {
    float lenght;
    float area;
    float compactness;
    float solidity;
    float roundness;
    float formFactor;

};

cv::Mat *krnlBoundEnhancer;

void imwrite_frame(const char *baseName, const char *ext, const uint frameNo, cv::Mat &frame) {
    //return;
    // pay attention: the compiler have to support dynamic sized array... alloca!!!
    int baseNameLen = strlen(baseName);
    char fileName[baseNameLen+4+4+1]; // 4 number, 4 ext (.png), 1 null
    strcat(fileName, baseName);
    sprintf(fileName+baseNameLen, "%04d", frameNo);
    char *fn = fileName+baseNameLen+4;
    for (int j = 0; ext[j]; j++)
        fn[j] = ext[j];
    fn[4] = '\0';
    std::cout << fileName << std::endl;
    imwrite(fileName, frame);
}

/**
 * @brief extractPowerDopplerBlob extract the power doppler signal.
 * @param img is the original image from the sonographer
 * @param bMode is the B-mode scan, filled in black where a flow is marked
 * @param maks is the binary mask of the doppler signal
 * @param cielabImg is the frame in cielab
 * @return the power doppler signal
 */
cv::Mat extractPowerDopplerBlob(cv::Mat& img, cv::Mat& bMode, cv::Mat &mask, cv::Mat &cielabImg) {

    // prerequisite: a 24-bit BGR image and a 24-bit Lab image
    assert (img.type() == CV_8UC3);
    assert(cielabImg.type() == CV_8UC3);

    // bMode contains the B-mode scan
    cv::cvtColor(img, bMode, CV_BGR2GRAY);
    cv::Mat boneMask, inverseBone;
    cv::inRange(cielabImg, std::vector<uchar> {0, 115, 115}, std::vector<uchar> {255, 141, 141}, boneMask);
    cv::bitwise_not ( boneMask, inverseBone);
    cv::Mat reducedCielab;
    cielabImg.copyTo(reducedCielab, inverseBone);
    cv::inRange(reducedCielab, std::vector<uchar> {0, 115, 138}, std::vector<uchar> {250, 195, 197}, mask);
    cv::morphologyEx(mask, mask,cv::MORPH_OPEN,cv::Mat::ones(3,3,CV_8UC1), cv::Point(-1, -1), 3);

    assert(bMode.type() == CV_8UC1);
    assert(mask.type() == CV_8UC1);

    cv::Mat pd;
    bMode.copyTo(pd, mask);
    bMode.copyTo(bMode, boneMask);

    return (pd);
}

void blob_extractor(cv::Mat &powerDopplerSignal)
{
    // Use a class or a function
    {
        std::vector < std::vector<cv::Point> > contours;
        cv::findContours(powerDopplerSignal, contours, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);
        for (size_t contourIdx = 0; contourIdx < contours.size(); contourIdx++)
        {
            /*
            cv::Mat blob(contours[contourIdx]);
            cv::drawContours(blob, contours, contourIdx, 255, CV_FILLED);
            cv::imshow("blob", blob);
            */
            cv::Moments moms = moments(cv::Mat(contours[contourIdx]));
            double area = moms.m00;
            if(area < 1.0)
                continue;
            double perimeter = cv::arcLength(cv::Mat(contours[contourIdx]), true);
            std::vector < cv::Point > hull;
            cv::convexHull(cv::Mat(contours[contourIdx]), hull);
            cv::Mat blobHull(hull);
            double hullArea = cv::contourArea(blobHull);

            cv::Point2d location = cv::Point2d(moms.m10 / moms.m00, moms.m01 / moms.m00);
            long int intensity = cv::sum(blobHull)[0];

            //compute blob radius
            double radius;
            {
                std::vector<double> dists;
                for (size_t pointIdx = 0; pointIdx < contours[contourIdx].size(); pointIdx++)
                {
                    cv::Point2d pt = contours[contourIdx][pointIdx];
                    dists.push_back(cv::norm(location - pt));
                }
                std::sort(dists.begin(), dists.end());
                radius = (dists[(dists.size() - 1) / 2] + dists[dists.size() / 2]) / 2.;
            }
            qDebug() << "Id: " << contourIdx;
            qDebug() << "Perimeter" << perimeter;
            qDebug() << "Area: " << area;
            qDebug() << "HullArea: " << hullArea;
            qDebug() << "Intensity: " << intensity;
            qDebug() << "Radius: " << radius;
        }
    }
    //exit(1);
}

void verticalLineEnhancement(cv::Mat& src) {
    int cols = src.cols;
    int rows = src.rows;
    for (int x = 0; x < cols; x++) {
        cv::Rect boundingBox(x, 0, 1, rows);
        cv::Mat line(src, boundingBox);
        cv::Scalar mean = cv::mean(line);
        line -= mean.val[0];
    }
}

void verticalLineCleaner(cv::Mat& src) {
    int diffLevel = 7; //< Warning: this ia a critical parameter
    //  and should be carefully calibrated
    const int rows = src.rows;
    const int cols = src.cols;
    const size_t step = src.step;
    uchar *const base = src.data;

    for (int x = 0; x < cols; x++) {
        int min = rows;
        int max = 0;
        uchar *level, *neighLevel;

        // find the first steep edge and set the min
        neighLevel = base + x;
        for (int y = 1; y < rows; y++, neighLevel= level) {
            level = neighLevel + step;
            if ((*level - *neighLevel) > diffLevel) {
                min = (neighLevel - base) / step;
                break;
            }
        }
        // if the upper edge has been found, look for the lower
        if (min > rows)
            break;
        neighLevel = base + x + rows * step;
        for (int y = rows; y > min; y--, neighLevel= level) {
            level = neighLevel - step;
            if ((*level - *neighLevel) > diffLevel) {
                max = (neighLevel - base) / step;
                break;
            }
        }
        // clean every pixel (between min and max)
        //for (int r = min + 1; r < max - 1; r++)
        for (int r = 0; r < rows; r++)
            *(base + x + r * step) = 0;
        // enforce min and max
        *(base + x + min * step) = 255;
        *(base + x + max * step) = 255;
        //std::cout << "col: " << x << "\tmin: " << min << "\tmax: " << max << std::endl;
    }
}

void findBone( cv::Mat& src, cv::Mat& dst) {
    // work on the half bottom part of the image
    cv::Rect thRect(0, 0, src.cols, src.rows - src.rows / 2);
    cv::Mat top = dst(thRect);
    cv::Rect bhRect(0, src.rows / 2, src.cols, src.rows / 2);
    cv::Mat bottom = dst(bhRect);
    top = cv::Mat::zeros(src.rows - src.rows / 2, src.cols, CV_8UC1);

    int kernelAvgSize = 7;
    cv::medianBlur(src(bhRect),bottom,kernelAvgSize);

    cv::filter2D(bottom, bottom, src.depth(), *krnlBoundEnhancer);
    cv::blur(bottom,bottom,cv::Size(3,3));
    for (int rep = 0; rep < 15; rep++) {
        verticalLineEnhancement(bottom);
    }
    assert (dst.type() == CV_8UC1);

    cv::Canny(bottom, bottom, 200, 250);
    cv::morphologyEx(bottom, bottom,cv::MORPH_CLOSE,cv::Mat::ones(3,3,CV_8UC1), cv::Point(-1, -1), 3);
    //cv::imshow("morph", bottom);

    std::vector<std::vector<cv::Point> > contours;
    cv::findContours( bottom, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
    int idx = 0;
    for (std::vector<std::vector<cv::Point> >::iterator c_iter = contours.begin();
         c_iter != contours.end(); c_iter++) {
        double area = cv::contourArea(*c_iter);
        // Filter by Area
#define MIN_AREA 50
        if (area < MIN_AREA) {
            contours.erase(c_iter--);
            continue;
        }
        /*
        // Get the moment
        cv::Moments mu;
        mu = moments( *c_iter, false );
        //  Get the mass center:
        cv::Point2f mc;
        mc = cv::Point2f( mu.m10/mu.m00 , mu.m01/mu.m00 );
        //qDebug() << mc.x << " " << mc.y;
        */
        idx++;
    }
    //verticalLineCleaner(bone);
    cv::drawContours(bottom, contours, -1, cv::Scalar::all(255),CV_FILLED);

    /*
    kmeansSegment kmeans(4);
    cv::Mat result = kmeans.segment(contorno);
    cv::Mat binary;
    cv::Canny(result, binary, 30, 90);
    cv::imshow("original", src);
    cv::imshow("binary", binary);
    cv::imshow( "clustered image", result);
    cv::imwrite("kmeans_binary.jpg", binary);
    cv::imwrite("kmeans.jpg", result);
    */
}

void findSkeleton(cv::Mat &src, cv::Mat &dst) {
    cv::Mat img;
    cv::threshold(src, img, 0, 255, cv::THRESH_BINARY | CV_THRESH_OTSU);
    cv::Mat *skel = new cv::Mat(img.size(), CV_8UC1, cv::Scalar(0));
    cv::Mat temp;
    cv::Mat eroded;

    cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));

    bool done;
    do
    {
        cv::erode(img, eroded, element);
        cv::dilate(eroded, temp, element); // temp = open(img)
        cv::subtract(img, temp, temp);
        cv::bitwise_or(*skel, temp, *skel);
        eroded.copyTo(img);

        done = (cv::countNonZero(img) == 0);
    } while (!done);
    dst = *skel;
}

void compose(
        cv::Mat &out,
        cv::Mat &roiRt, cv::Mat &sum,
        cv::Mat &pdRt, cv::Mat &pdSum,
        cv::Mat &boneRt, cv::Mat &boneSum) {
    int w = roiRt.cols,
        h = roiRt.rows;
    // real time frame
    cv::Rect scr0r = cv::Rect(0, 0, w, h);
    cv::Mat scr0 = out(scr0r);
    roiRt.copyTo(scr0);
    // real time power doppler
    cv::Rect scr2r = cv::Rect(0, h, w, h);
    cv::Mat scr2 = out(scr2r);
    pdRt.copyTo(scr2);
    // real time bone
    cv::Rect scr4r = cv::Rect(0, 2*h, w, h);
    cv::Mat scr4 = out(scr4r);
    boneRt.copyTo(scr4);
    // summarization, complete
    cv::Rect scr1r = cv::Rect(w, 0, w, h);
    cv::Mat scr1 = out(scr1r);
    sum.copyTo(scr1);
    // summariation, power doppler
    cv::Rect scr3r = cv::Rect(w, h, w, h);
    cv::Mat scr3 = out(scr3r);
    pdSum.copyTo(scr3);
    // summarization, bone
    cv::Rect scr5r = cv::Rect(w, 2*h, w, h);
    cv::Mat scr5 = out(scr5r);
    boneSum.copyTo(scr5);
    // registration, real time
    cv::Rect scr6r = cv::Rect(0, 3*h, w, h);
    cv::Mat scr6 = out(scr6r);
    rtImages.sonographyReg.copyTo(scr6);

    cv::line(out, cv::Point(0,   0), cv::Point(2*w,   0), cv::Scalar(255,255,255));
    cv::line(out, cv::Point(0,   h), cv::Point(2*w,   h), cv::Scalar(255,255,255));
    cv::line(out, cv::Point(0, 2*h), cv::Point(2*w, 2*h), cv::Scalar(255,255,255));
    cv::line(out, cv::Point(0, 3*h), cv::Point(2*w, 3*h), cv::Scalar(255,255,255));
    cv::line(out, cv::Point(0, 4*h), cv::Point(2*w, 4*h), cv::Scalar(255,255,255));
    cv::line(out, cv::Point(  0, 0), cv::Point(  0, 4*h), cv::Scalar(255,255,255));
    cv::line(out, cv::Point(  w, 0), cv::Point(  w, 4*h), cv::Scalar(255,255,255));
    cv::line(out, cv::Point(2*w, 0), cv::Point(2*w, 4*h), cv::Scalar(255,255,255));

    cv::imshow("out", out);
    *(context.outputVideoPtr) << out;
    cv::waitKey(200);
}

void putHeading(cv::Mat img, std::string text1, std::string text2, int right = 0) {
    int fontFace = CV_FONT_HERSHEY_SIMPLEX;
    double fontScale = 2;
    int thickness = 1;
    int baseline=0;

    cv::Size textSize = cv::getTextSize(text1, fontFace,
                                fontScale, thickness, &baseline);
    baseline += thickness;
    // center the text1
    cv::Point textOrg((img.cols - textSize.width)/2,
                  (img.rows + textSize.height)/2 - 1.5*baseline);
    cv::putText(img, text1, textOrg, fontFace, fontScale,
            cv::Scalar::all(255), thickness, 8);
    textSize = cv::getTextSize(text2, fontFace,
                                fontScale, thickness, &baseline);
    baseline += thickness;

    // center the text2
    textOrg.x = (img.cols - textSize.width)/2;
    textOrg.y = (img.rows + textSize.height)/2 + 1.5*baseline;

    // then put the text itself
    cv::putText(img, text2, textOrg, fontFace, fontScale,
            cv::Scalar::all(255), thickness, 8);

    // box
    cv::Scalar color;
    if (right) {
        color = cv::Scalar(0, 255, 0);
    } else {
        color = cv::Scalar(0, 0, 255);
    }
    cv::rectangle(img, cv::Point(0, 0), cv::Point(img.cols, img.rows), color, 3);
}

void sceneHeadings( int width, int height, cv::Mat &frame) {
    cv::Mat sonographyRt(height, width, CV_8UC3, cv::Scalar::all(0)),
            sonographySummary(height, width, CV_8UC3, cv::Scalar::all(0)),
            dopplerSignalRt(height, width, CV_8UC3, cv::Scalar::all(0)),
            dopplerSignalSummary(height, width, CV_8UC3, cv::Scalar::all(0)),
            boneSignalRt(height, width, CV_8UC3, cv::Scalar::all(0)),
            boneSignalSummary(height, width, CV_8UC3, cv::Scalar::all(0));

    putHeading(sonographyRt, "Sonography", "real time", 0);
    putHeading(sonographySummary, "Sonography", "summary", 1);
    putHeading(dopplerSignalRt, "Doppler signal", "real time", 0);
    putHeading(dopplerSignalSummary, "Doppler signal", "summary", 1);
    putHeading(boneSignalRt, "Bone", "real time", 0);
    putHeading(boneSignalSummary, "Bone", "summary", 1);
    for (int j = 0; j < 1; j++) {
        compose( frame,
                 sonographyRt, sonographySummary,
                 dopplerSignalRt, dopplerSignalSummary,
                 boneSignalRt, boneSignalSummary);
    }
}

void createVideoFrame(
        cv::Mat &outFrame
        ) {
    cv::Mat dRt, bRt,
            pdSum8bit, boneSum8bit, tmp,
            dopplerSum, boneSum, summarization;
    cv::applyColorMap(rtImages.dopplerSignal, dRt, cv::COLORMAP_HOT);
    cv::cvtColor(rtImages.boneSignal, bRt, CV_GRAY2BGR);

    // doppler summarization
    tmp = summaryImages.dopplerSignal / (context.frame.sequenceNo + 1);
    tmp.convertTo(pdSum8bit, CV_8U);
    cv::applyColorMap(pdSum8bit, dopplerSum, cv::COLORMAP_HOT);

    tmp = summaryImages.boneSignal / (context.frame.sequenceNo + 1);
    tmp.convertTo(boneSum8bit, CV_8U);
    cv::cvtColor(boneSum8bit, boneSum, CV_GRAY2BGR);
    // cv::imshow("boneSum", boneSum8bit);

    cv::add(dopplerSum, boneSum, summarization);

    compose(outFrame, rtImages.sonography, summarization,
                    dRt, dopplerSum,
                    bRt, boneSum);
}

void summarize() {
    cv::Mat pdFloat;
    rtImages.dopplerSignal.convertTo(pdFloat, CV_64FC1);
    cv::Mat boneFloat;
    rtImages.boneSignal.convertTo(boneFloat, CV_64FC1);
    //cv::addWeighted(boneFloat, 2./3, rtImages.boneSignal, 1./3, 0., boneFloat);
    if (context.frame.sequenceNo == 0) {
        summaryImages.dopplerSignal = pdFloat.clone();
        summaryImages.boneSignal = boneFloat.clone();
    } else {
        cv::add(summaryImages.dopplerSignal, pdFloat, summaryImages.dopplerSignal);
        cv::add(summaryImages.boneSignal, boneFloat, summaryImages.boneSignal);
    }
}

void showDifference(const cv::Mat& image1, const cv::Mat& image2, const char* title)
{
    cv::Mat img1, img2;
    image1.convertTo(img1, CV_32FC3);
    image2.convertTo(img2, CV_32FC3);
    if(img1.channels() != 1)
        cv::cvtColor(img1, img1, CV_RGB2GRAY);
    if(img2.channels() != 1)
        cv::cvtColor(img2, img2, CV_RGB2GRAY);

    cv::Mat imgDiff;
    img1.copyTo(imgDiff);
    imgDiff -= img2;
    imgDiff /= 2.f;
    imgDiff += 128.f;

    cv::Mat imgSh;
    imgDiff.convertTo(imgSh, CV_8UC3);
    cv::imshow(title, imgSh);
}

void imageRegister() {
    if (context.frame.sequenceNo == 0) {
        rtImages.sonography.copyTo(registrationImages.templateBone,
                                   rtImages.boneMask);
        // Convert to double, 3 channels
        registrationImages.templateBone.convertTo(registrationImages.templateBone, CV_64FC3);
    } else {
        // Register
        cv::reg::MapperGradProj mapper;
        cv::reg::MapperPyramid mappPyr(mapper);
        cv::Ptr<cv::reg::Map> mapPtr;

        cv::Mat img2;
        rtImages.sonography.copyTo(img2,
                                   rtImages.boneMask);
        // Convert to double, 3 channels
        img2.convertTo(img2, CV_64FC3);

        mappPyr.calculate(registrationImages.templateBone,
                          img2,
                          mapPtr);

        cv::reg::MapProjec* mapProj = dynamic_cast<cv::reg::MapProjec*>(mapPtr.obj);
        mapProj->normalize();
        registrationImages.mapProj = *mapProj;
        mapProj->inverseWarp(rtImages.boneSignal, rtImages.boneSignal);
        mapProj->inverseWarp(rtImages.dopplerSignal, rtImages.dopplerSignal);
        mapProj->inverseWarp(rtImages.sonography, rtImages.sonographyReg);
        // Print result
        //std::cout << std::endl << "--- Testing projective transformation mapper ---" << std::endl;
        //std::cout << cv::Mat(mapProj->getProjTr()) << std::endl;

        // Display registration accuracy
        //cv::Mat dest;
        //mapProj->inverseWarp(img2, dest);
        //static const char* DIFF_IM = "Image difference";
        //static const char* DIFF_REGPIX_IM = "Image difference: pixel registered";
        // showDifference(registrationImages.templateBone, dest, DIFF_REGPIX_IM);
    }
}

void process(Parameter &p) {
    // the OpenCV video capture object
    cv::VideoCapture capture(p.fileName.toStdString());
    // the bounding box
    cv::Rect boundingBox(p.roi.x, p.roi.y, p.roi.w, p.roi.h);
    if (!capture.isOpened()) {
        exit (1);
    }

    //! the video frame
    cv::Mat frame;
    //! the b-mode
    cv::Mat Bmode;
    //! a mask of the power doppler signal
    cv::Mat pdMask(boundingBox.height, boundingBox.width, CV_8UC1);
    cv::Mat boneMask(boundingBox.height, boundingBox.width, CV_8UC1);
    //! The image in the CIE L*a*b* space
    cv::Mat cielabImg;
    // the video frame
    cv::Mat outFrame(boundingBox.height*4, boundingBox.width*2, CV_8UC3);

    context.frame.sequenceNo = 0;

    cv::Size s = cv::Size((int) boundingBox.width*2,
                     (int) boundingBox.height*3);
    QString outFileName = p.fileName;
    outFileName.replace(".avi", "_processed.avi");
    // int ex = static_cast<int>(capture.get(CV_CAP_PROP_FOURCC));
    int ex = 877677894;
    try {
        context.outputVideoPtr = new cv::VideoWriter(outFileName.toStdString(), ex, capture.get(CV_CAP_PROP_FPS), s, true);
    } catch( cv::Exception& e ) {
        const char* err_msg = e.what();
        std::cout << "exception caught: " << err_msg << std::endl;
    }

    if (!context.outputVideoPtr->isOpened()) {
        std::cout  << "Could not open the output video for write: " << outFileName.toStdString() << std::endl;
        exit (1);
    }

    sceneHeadings(boundingBox.width, boundingBox.height, outFrame);

    // while there exists a next frame
    while (capture.read(frame)) {
        rtImages.sonography = cv::Mat(frame, boundingBox);
        cv::cvtColor(rtImages.sonography, cielabImg, CV_BGR2Lab);
        // get the artifacts and remove them from the frame
        rtImages.dopplerSignal = extractPowerDopplerBlob(rtImages.sonography, Bmode, pdMask, cielabImg);
        findBone(Bmode, boneMask);
        Bmode.copyTo(rtImages.boneSignal, boneMask);
        // per Adriano
        cv::Mat BmodeMinusPd = Bmode - pdMask;
	const char *fn = basename(p.fileName.toStdString().data());
	std::cout << fn << std::endl;
        imwrite_frame(fn, ".pgm", context.frame.sequenceNo, BmodeMinusPd);
        // fine
        imageRegister();
        summarize();
        createVideoFrame(outFrame);
        /* end find bone */

        context.frame.sequenceNo++;
    }
    capture.release();
    delete context.outputVideoPtr;
    exit(0);
}

bool readParameters(Parameter &p, const char *fileName) {
    QFile configFile(fileName);

    if (!configFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open config file.");
        return false;
    }

    QByteArray configData = configFile.readAll();
    QJsonDocument configDoc(QJsonDocument::fromJson(configData));
    QJsonObject configObject = configDoc.object();

    p.fileName = configObject["fileName"].toString();
    QJsonObject roiObject = configObject["ROI"].toObject();
    p.roi.x = roiObject["x"].toInt();
    p.roi.y = roiObject["y"].toInt();
    p.roi.w = roiObject["w"].toInt();
    p.roi.h = roiObject["h"].toInt();

    return true;
}

void krnlBoundEnhancerSetup(void)
{
    /*
    krnlBoundEnhancer = new cv::Mat(2,2,CV_32F,cv::Scalar(1.0));
    krnlBoundEnhancer->at<float>(0,0) = -1.0;
    krnlBoundEnhancer->at<float>(0,1) = -1.0;
    krnlBoundEnhancer->at<float>(1,0) =  1.0;
    krnlBoundEnhancer->at<float>(1,1) =  1.0;
    */
    krnlBoundEnhancer = new cv::Mat(5,5,CV_32F,cv::Scalar(1.0));
    krnlBoundEnhancer->at<float>(0,0) = -2.0;
    krnlBoundEnhancer->at<float>(0,1) = -2.0;
    krnlBoundEnhancer->at<float>(0,2) = -2.0;
    krnlBoundEnhancer->at<float>(0,3) = -2.0;
    krnlBoundEnhancer->at<float>(0,4) = -2.0;
    krnlBoundEnhancer->at<float>(1,0) = -1.0;
    krnlBoundEnhancer->at<float>(1,1) =  2.0;
    krnlBoundEnhancer->at<float>(1,2) =  4.0;
    krnlBoundEnhancer->at<float>(1,3) =  2.0;
    krnlBoundEnhancer->at<float>(1,4) = -1.0;
    krnlBoundEnhancer->at<float>(2,0) =  0.0;
    krnlBoundEnhancer->at<float>(2,1) =  4.0;
    krnlBoundEnhancer->at<float>(2,2) =  8.0;
    krnlBoundEnhancer->at<float>(2,3) =  4.0;
    krnlBoundEnhancer->at<float>(2,4) =  0.0;
    krnlBoundEnhancer->at<float>(3,0) = -1.0;
    krnlBoundEnhancer->at<float>(3,1) =  2.0;
    krnlBoundEnhancer->at<float>(3,2) =  4.0;
    krnlBoundEnhancer->at<float>(3,3) =  2.0;
    krnlBoundEnhancer->at<float>(3,4) = -1.0;
    krnlBoundEnhancer->at<float>(4,0) = -2.0;
    krnlBoundEnhancer->at<float>(4,1) = -2.0;
    krnlBoundEnhancer->at<float>(4,2) = -2.0;
    krnlBoundEnhancer->at<float>(4,3) = -2.0;
    krnlBoundEnhancer->at<float>(4,4) = -2.0;
    int norm = 0;
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            norm+= krnlBoundEnhancer->at<float>(i,j);
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            krnlBoundEnhancer->at<float>(i,j)/=norm;
    return;
}

int main(int argc, char *argv[])
{
    Parameter p;
    const char *fileName;

    if (argc < 2) {
        qWarning(QString("Usage: %1 <config.json>").arg(argv[0]).toLocal8Bit());
        fileName = "config.json";
    } else {
        fileName = argv[1];
    }
    if (!readParameters(p, fileName))
        return 1;

    //Context context;
    krnlBoundEnhancerSetup();
    process(p);

    return 0;
}
