#-------------------------------------------------
#
# Project created by QtCreator 2013-12-10T18:26:50
#
#-------------------------------------------------

QT       += core gui opengl sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = selector
TEMPLATE = app

SOURCES += \
    main.cpp \
    mainwindow.cpp
    
HEADERS  += \
    mainwindow.h 
FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc

OCV_PREFIX=opencv_
OCV_LIB = core \
          highgui \
          imgproc
unix {
    for(ocvlib, OCV_LIB) {
        LIBS += -l$$OCV_PREFIX$$ocvlib
    }
    # I'm compiling for myself... i.e. for my core-avx2
    QMAKE_CXXFLAGS+=-march=core-avx2
    QMAKE_CXXFLAGS+=-Wno-reorder
    #QMAKE_CFLAGS_AVX2
}
