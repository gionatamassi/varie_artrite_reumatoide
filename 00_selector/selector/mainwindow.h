#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Old C standard library
#include <stdio.h>
// C++ Standard Template Library
#include <iostream>
// QT (5.4)
#include <QtCore>
#include <QMainWindow>
#include <QFileDialog>
#include <QSettings>
#include <QRubberBand>
#include <QMouseEvent>
#include <QDebug>
#include <QMessageBox>
// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    enum tabEnum {
        FILE_CHOOSER = 0,
        ROI_SELECTOR,
        END
    };

public slots:
    void roiSliderControlShow(bool show);
    void tabShow(tabEnum t);
    void btnPrevShow(bool enabled);
    void btnNextShow(bool enabled);

private:
    Ui::MainWindow *ui;
    cv::VideoCapture m_video;
    QString m_fileName;

    QString m_settingsFileRunDir;
    QString m_settingsFileVideoDir;

    QRubberBand *m_rubberBand;
    QPoint m_referencePoint,
        m_topLeftCoordinate, m_bottomRightCoordinate;
    bool m_bbSpinBoxEnable;
    tabEnum currentTab;
    cv::Mat m_firstFrame;

    char *m_outputDirectory;

    void frameExtract(QRect bb);
    bool loadFile(const QString &fileName);
    void loadSettings();
    void saveSettings();
    void updateBB(int x, int y);

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private slots:
    //void on_btnNext_clicked();
    void on_btnChoose_clicked();
    void openFile();
    void updateBBSpinX(int x);
    void updateBBSpinY(int y);
    void updateBBSpinW(int w);
    void updateBBSpinH(int h);

    void on_btnNext_clicked();
};

#endif // MAINWINDOW_H
