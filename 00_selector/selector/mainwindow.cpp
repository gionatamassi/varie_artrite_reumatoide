#include "mainwindow.h"
#include "ui_mainwindow.h"

/**
 * @brief MainWindow::MainWindow
 * @param parent
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_rubberBand(0),
    m_bbSpinBoxEnable(false),
    currentTab(FILE_CHOOSER),
    m_outputDirectory(0)
{
    ui->setupUi(this);
    ui->btnChoose->setIcon(QIcon::fromTheme("file-open"));

    ui->tabWidget->setFocusPolicy(Qt::StrongFocus);
    ui->tabWidget->setCurrentIndex(0);
    // disable all tabs except the first one
    ui->tabWidget->tabBar()->setEnabled(false);
    m_settingsFileRunDir =  "rapda.ini";
    QSettings settings(m_settingsFileRunDir, QSettings::IniFormat);
    if(settings.contains("filename")) {
        ui->lneVideoPath->setText(settings.value("filename").toString());
        ui->lneFrameDir->setText(settings.value("framesdir").toString());
    }

    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(openFile()));
    connect(ui->spinX, SIGNAL(valueChanged(int)), this, SLOT(updateBBSpinX(int)));
    connect(ui->spinY, SIGNAL(valueChanged(int)), this, SLOT(updateBBSpinY(int)));
    connect(ui->spinWidth, SIGNAL(valueChanged(int)), this, SLOT(updateBBSpinW(int)));
    connect(ui->spinHeight, SIGNAL(valueChanged(int)), this, SLOT(updateBBSpinH(int)));
    }

/**
 * @brief MainWindow::~MainWindow
 */
MainWindow::~MainWindow()
{
    QSettings settings(m_settingsFileRunDir, QSettings::IniFormat);
    settings.setValue("filename", ui->lneVideoPath->text());
    settings.setValue("framesdir", ui->lneFrameDir->text());
    delete ui;
}

/**
 * @brief MainWindow::frameExtract
 * @param video
 * @param bb
 * @param dir
 * @param baseName
 */
void
MainWindow::frameExtract(QRect bb) {
    cv::Rect boundingBox(
        bb.x(), bb.y(), bb.width(), bb.height()
    );
    assert(boundingBox.area() > 0);
}

/**
 * @brief MainWindow::openFile
 */
void MainWindow::openFile() {
    QString fileName, currentPath, filter, defaultFilter;

    defaultFilter.append("File video avi (*.avi);;");
    filter.append(defaultFilter);
    filter.append("File video mov (*.mov);;");
    filter.append("Tutte le estensioni (*);;");
    fileName = QFileDialog::getOpenFileName(this, tr("Open"), currentPath, filter);
    QFileInfo fi(fileName);
    ui->lneVideoPath->setText(fileName);
    ui->lneFrameDir->setText(fi.path() +  QDir::separator() + fi.baseName());
    m_fileName = fileName;
}

/**
 * @brief MainWindow::loadFile
 * @param fileName
 */
bool MainWindow::loadFile(const QString &fileName)
{
    m_fileName = fileName;
    m_video=cv::VideoCapture(fileName.toStdString());
    if (!m_video.isOpened())
        return (false);
    // Settings
    QFileInfo fi(fileName);
    m_settingsFileVideoDir =  fi.path() +  QDir::separator() + "rapda.ini";
    loadSettings();
    m_video >> m_firstFrame;
    cv::Mat imageRGB;
    // change color channel ordering
    cv::cvtColor(m_firstFrame, imageRGB, CV_BGR2RGB);
    // Qt image
    QImage img= QImage((const unsigned char*)(imageRGB.data),
                       imageRGB.cols,imageRGB.rows,QImage::Format_RGB888);
    // display it using a qlabel
    ui->lblFirstFrame->setPixmap(QPixmap::fromImage(img));
    // resize the qlabel to fit the image
    ui->lblFirstFrame->resize(ui->lblFirstFrame->pixmap()->size());
    // add an invisible rubber band
    if(!m_rubberBand) {
        m_rubberBand = new QRubberBand(QRubberBand::Rectangle, ui->lblFirstFrame);
    }
    m_rubberBand->setGeometry(
        m_topLeftCoordinate.x(),
        m_topLeftCoordinate.y(),
        ui->spinWidth->value(),
        ui->spinHeight->value()
    );
    QPalette pal;
    pal.setBrush(QPalette::Highlight, QBrush(Qt::red));
    m_rubberBand->setPalette(pal);
    m_rubberBand->show();

    m_video.release();
    m_video = cv::VideoCapture(fileName.toStdString());
    return (true);
}

/**
 * @brief MainWindow::loadSettings
 */
void MainWindow::loadSettings()
{
    m_bbSpinBoxEnable = false;
    QSettings settings(m_settingsFileVideoDir, QSettings::IniFormat);
    if (settings.contains("x")) {
        ui->spinX->setValue(settings.value("x").toInt());
        m_referencePoint.setX(ui->spinX->value());
        ui->spinY->setValue(settings.value("y").toInt());
        m_referencePoint.setY(ui->spinY->value());
        m_topLeftCoordinate = m_referencePoint;
        ui->spinWidth->setValue(settings.value("width").toInt());
        m_bottomRightCoordinate.setX(
            m_topLeftCoordinate.x() + ui->spinWidth->value());
        ui->spinHeight->setValue(settings.value("height").toInt());
        m_bottomRightCoordinate.setY(
            m_topLeftCoordinate.y() + ui->spinHeight->value());
    }
    m_bbSpinBoxEnable = true;
}

/**
 * @brief MainWindow::saveSettings
 */
void MainWindow::saveSettings()
{
    QSettings settings(m_settingsFileVideoDir, QSettings::IniFormat);
    settings.setValue("x", ui->spinX->value());
    settings.setValue("y", ui->spinY->value());
    settings.setValue("width", ui->spinWidth->value());
    settings.setValue("height", ui->spinHeight->value());
    settings.sync();
}

/**
 * @brief MainWindow::mousePressEvent
 * @param event
 */
void MainWindow::mousePressEvent(QMouseEvent *event) {
    // QMutexLocker locker(gpMutex);

    if (this->ui->tabWidget->currentIndex() != 1) {
        return;
    }
    if (!ui->lblFirstFrame->rect().contains(event->pos()))
        return;
    m_bbSpinBoxEnable = false;
    if(m_rubberBand)
        m_rubberBand->hide();
    m_referencePoint =
        m_topLeftCoordinate =
            m_bottomRightCoordinate =
                ui->lblFirstFrame->mapFromGlobal(
                    this->mapToGlobal(event->pos()));
    if(!m_rubberBand)
        m_rubberBand = new QRubberBand(QRubberBand::Rectangle, ui->lblFirstFrame);//new rectangle band
    // m_rubberBand->hide();// hide on mouse Release
    QPalette pal;
    pal.setBrush(QPalette::Highlight, QBrush(Qt::green));
    m_rubberBand->setPalette(pal);

    m_rubberBand->clearMask();

    m_rubberBand->setGeometry(QRect(m_referencePoint,
                                    m_referencePoint));//Area Bounding

    m_rubberBand->show();
    //qDebug() << "First press: " << ui->lblFirstFrame->mapFromGlobal(this->mapToGlobal(event->pos()));
}

/**
 * @brief MainWindow::mouseMoveEvent
 * @param event
 */
void MainWindow::mouseMoveEvent(QMouseEvent *event) {
    // QMutexLocker locker(gpMutex);

    if (this->ui->tabWidget->currentIndex() != 1) {
        return;
    }
    QPoint cursor = ui->lblFirstFrame->mapFromGlobal(this->mapToGlobal(event->pos()));
    updateBB(cursor.x(), cursor.y());
}

/**
 * @brief MainWindow::mouseReleaseEvent
 * @param event
 */
void MainWindow::mouseReleaseEvent(QMouseEvent *event) {
    if (this->ui->tabWidget->currentIndex() != 1) {
        return;
    }
    QPalette pal;
    pal.setBrush(QPalette::Highlight, QBrush(Qt::blue));
    m_rubberBand->setPalette(pal);
    m_bbSpinBoxEnable = true;
}

/**
 * @brief MainWindow::updateBB
 * @param x
 * @param y
 */
void MainWindow::updateBB(int x, int y)
{
    // QMutexLocker locker(gpMutex);

    int p_x, p_y;
    p_x = m_referencePoint.x();
    p_y = m_referencePoint.y();
    m_topLeftCoordinate.setX(std::min(p_x, x));
    m_topLeftCoordinate.setY(std::min(p_y, y));

    m_bottomRightCoordinate.setX(std::max(p_x, x));
    m_bottomRightCoordinate.setY(std::max(p_y, y));

    m_rubberBand->setGeometry(QRect(m_topLeftCoordinate,
                                    m_bottomRightCoordinate));//Area Bounding
    ui->spinX->setValue(m_topLeftCoordinate.x());
    ui->spinY->setValue(m_topLeftCoordinate.y());
    ui->spinWidth->setValue(
        m_bottomRightCoordinate.x() - m_topLeftCoordinate.x());
    ui->spinHeight->setValue(
        m_bottomRightCoordinate.y() - m_topLeftCoordinate.y());
}

/**
 * @brief MainWindow::updateBBSpinX
 * @param x
 */
void MainWindow::updateBBSpinX(int x)
{
    if (!m_bbSpinBoxEnable)
        return;
    m_topLeftCoordinate.setX(x);
    m_referencePoint = m_topLeftCoordinate;
    updateBB(m_bottomRightCoordinate.x(),
             m_bottomRightCoordinate.y());
}

/**
 * @brief MainWindow::updateBBSpinY
 * @param y
 */
void MainWindow::updateBBSpinY(int y)
{
    if (!m_bbSpinBoxEnable)
        return;
    m_topLeftCoordinate.setY(y);
    m_referencePoint = m_topLeftCoordinate;
    updateBB(m_bottomRightCoordinate.x(),
             m_bottomRightCoordinate.y());
}

/**
 * @brief MainWindow::updateBBSpinW
 * @param w
 */
void MainWindow::updateBBSpinW(int w)
{
    if (!m_bbSpinBoxEnable)
        return;
    m_referencePoint = m_topLeftCoordinate;
    m_bottomRightCoordinate.setX(
        m_topLeftCoordinate.x()+w);
    updateBB(m_bottomRightCoordinate.x(),
             m_bottomRightCoordinate.y());
}

/**
 * @brief MainWindow::updateBBSpinH
 * @param h
 */
void MainWindow::updateBBSpinH(int h)
{
    if (!m_bbSpinBoxEnable)
        return;
    m_referencePoint = m_topLeftCoordinate;
    m_bottomRightCoordinate.setY(
        m_topLeftCoordinate.y()+h);
    updateBB(m_bottomRightCoordinate.x(),
             m_bottomRightCoordinate.y());
}

void MainWindow::on_btnNext_clicked()
{
    bool fail = true;
    currentTab = static_cast<tabEnum>(ui->tabWidget->currentIndex());
    switch (currentTab) {
    case FILE_CHOOSER:
        if (ui->lneVideoPath->text().size() > 0) {
            fail = false;
            loadFile(this->ui->lneVideoPath->text());
        }
        break;
    case ROI_SELECTOR:
    {
        saveSettings();
        // Low level ancient style :(
        m_outputDirectory = (char *)malloc(ui->lneFrameDir->text().length()+1);
        strcpy(m_outputDirectory, ui->lneFrameDir->text().toLocal8Bit().data());
        frameExtract(
                     QRect(m_topLeftCoordinate, m_bottomRightCoordinate)
                     );
        m_video.release();
        fail = false;
        break;
    }
    case END:
        fail = false;
        break;
    default:
        break;
    }
    currentTab = static_cast<tabEnum>(static_cast<unsigned>(currentTab) + 1);
    if (!fail && currentTab < END) {
        ui->tabWidget->setCurrentIndex(currentTab);
    }

}

/**
 * @brief MainWindow::on_btnChoose_clicked
 */
void MainWindow::on_btnChoose_clicked()
{
    //QMutexLocker locker(gpMutex);
    openFile();
}

void MainWindow::roiSliderControlShow(bool show)
{
    // Region of interest controls
    for (int i = 0; i < ui->glROIsliders->count();i++) {
        if (QWidgetItem *myItem = dynamic_cast <QWidgetItem*>(ui->glROIsliders->itemAt(i))) {
            if (show)
                myItem->widget()->show();
            else
                myItem->widget()->hide();
        }
    }
}

void MainWindow::tabShow(MainWindow::tabEnum t)
{
    ui->tabWidget->setCurrentIndex(t);
}

void MainWindow::btnPrevShow(bool enabled)
{
    if (enabled)
        ui->btnPrevious->show();
    else
        ui->btnPrevious->hide();
}

void MainWindow::btnNextShow(bool enabled)
{
    if (enabled)
        ui->btnNext->show();
    else
        ui->btnNext->hide();
}
