#include <QCoreApplication>
#include <QString>
#include <QDebug>

#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

/// Global Variables
const int ch1_slider_max = 255;
const int ch2_slider_max = 255;
const int ch3_slider_max = 255;

int ch1_slider_lb = 0;
int ch1_slider_ub = 255;
int ch2_slider_lb = 0;
int ch2_slider_ub = 255;
int ch3_slider_lb = 0;
int ch3_slider_ub = 255;

cv::Mat bgr;
cv::Mat lab;

void on_trackbar( int, void* )
{
    std::vector<int> lb = {ch1_slider_lb, ch2_slider_lb, ch3_slider_lb};
    std::vector<int> ub = {ch1_slider_ub, ch2_slider_ub, ch3_slider_ub};
    cv::Mat mask;
    cv::inRange(lab, lb, ub, mask);
    cv::imshow("mask", mask);
    cv::Mat filtered;
    bgr.copyTo(filtered, mask);
    cv::imshow( "filtered", filtered );
}

int main(int argc, char *argv[])
{
    const std::string videoName = "../../video/long/0002.avi";
    cv::VideoCapture capture(videoName);
    capture >> bgr;
    capture.release();
    cv::cvtColor(bgr, lab, CV_BGR2Lab);

    /// Create Windows
    cv::namedWindow("filtered", 1);
    cv::imshow("filtered", bgr);

     /// Create Trackbars
     cv::createTrackbar( "ch1_lb", "filtered", &ch1_slider_lb, ch1_slider_max, on_trackbar );
     cv::createTrackbar( "ch1_ub", "filtered", &ch1_slider_ub, ch1_slider_max, on_trackbar );
     cv::createTrackbar( "ch2_lb", "filtered", &ch2_slider_lb, ch2_slider_max, on_trackbar );
     cv::createTrackbar( "ch2_up", "filtered", &ch2_slider_ub, ch2_slider_max, on_trackbar );
     cv::createTrackbar( "ch3_lb", "filtered", &ch3_slider_lb, ch3_slider_max, on_trackbar );
     cv::createTrackbar( "ch3_up", "filtered", &ch3_slider_ub, ch3_slider_max, on_trackbar );
     on_trackbar(0, 0);

     /// Wait until user press some key
     cv::waitKey(0);

     return 0;
}
