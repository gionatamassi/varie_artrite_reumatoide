#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

/// Global Variables
const int ch1_slider_max = 255;
const int ch2_slider_max = 255;
const int ch3_slider_max = 255;

int ch1_slider_lb = 0;
int ch1_slider_ub = 255;
int ch2_slider_lb = 0;
int ch2_slider_ub = 255;
int ch3_slider_lb = 0;
int ch3_slider_ub = 255;

cv::Mat bgr;
cv::Mat lab;

void on_trackbar( int, void* )
{
    std::vector<int> lb = {ch1_slider_lb, ch2_slider_lb, ch3_slider_lb};
    std::vector<int> ub = {ch1_slider_ub, ch2_slider_ub, ch3_slider_ub};
    cv::Mat mask, inverseMask;
    cv::inRange(lab, lb, ub, mask);
    inverseMask = 255 - mask;
    cv::imshow("mask", mask);
    cv::imshow("inverseMask", inverseMask);

    cv::Mat filtered;
    bgr.copyTo(filtered, mask);
    cv::imshow( "filtered", filtered );

    cv::Mat grayLevels;
    bgr.copyTo(grayLevels, inverseMask);
    cv::imshow( "grayLevels", grayLevels );
    cv::imwrite("video_.ppm", grayLevels);

}

int main(int argc, char *argv[])
{
    const char *arg = "1";
    if (argc > 1)
        arg =argv[1];

    const std::string videoNamePrefix = "../../video/long/000";
    const std::string videoNameExtension = ".avi";

    //! The video file name
    std::string videoName = videoNamePrefix + arg + videoNameExtension;
    std::cout << videoName << std::endl;

    cv::VideoCapture capture(videoName);
    capture >> bgr;
    capture.release();
    cv::cvtColor(bgr, lab, CV_BGR2Lab);

    /// Create Windows
    cv::namedWindow("filtered", 1);
    cv::imshow("filtered", bgr);

     /// Create Trackbars
     // name, windows name, value, max, onChange
     cv::createTrackbar( "ch1_lb", "filtered", &ch1_slider_lb, ch1_slider_max, on_trackbar );
     cv::createTrackbar( "ch1_ub", "filtered", &ch1_slider_ub, ch1_slider_max, on_trackbar );
     cv::createTrackbar( "ch2_lb", "filtered", &ch2_slider_lb, ch2_slider_max, on_trackbar );
     cv::createTrackbar( "ch2_ub", "filtered", &ch2_slider_ub, ch2_slider_max, on_trackbar );
     cv::createTrackbar( "ch3_lb", "filtered", &ch3_slider_lb, ch3_slider_max, on_trackbar );
     cv::createTrackbar( "ch3_ub", "filtered", &ch3_slider_ub, ch3_slider_max, on_trackbar );

     cv::setTrackbarPos("ch1_lb", "filtered", 0);
     cv::setTrackbarPos("ch1_ub", "filtered", 255);

     cv::setTrackbarPos("ch2_lb", "filtered", 115);
     cv::setTrackbarPos("ch2_ub", "filtered", 195);

     cv::setTrackbarPos("ch3_lb", "filtered", 138);
     cv::setTrackbarPos("ch3_ub", "filtered", 197);

     on_trackbar(0, 0);

     /// Wait until user press some key
     cv::waitKey(0);

     return 0;
}
